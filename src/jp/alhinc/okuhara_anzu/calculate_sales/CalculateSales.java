package jp.alhinc.okuhara_anzu.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		Map<String, String> branchNames = new HashMap<>();//
		Map<String, Long> branchSales = new HashMap<>();

		//ｺﾏﾝﾄﾞﾗｲﾝ引数ﾃﾞｨﾚｸﾄﾘ情報
		System.out.println("ここにあるファイルを開きます =>" + args[0]);

		//【エラー処理】ｺﾏﾝﾄﾞﾗｲﾝ引数が渡されているか確認。
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		if (!input(args[0], "branch.lst", "支店定義", "^[0-9]{3}$", branchNames, branchSales)) {
			return;
		}

		//filesという配列に指定したパスに存在する全てのファイル(または、ディレクトリ)の情報が格納。
		File[] files = new File(args[0]).listFiles();

		//filesに条件に当てはまったファイルが格納される。//<>の中はﾃﾞｰﾀ型(参照型)。先にファイルの情報を格納する List(ArrayList) を宣言。
		List<File> rcdFiles = new ArrayList<>();

		//filesの数だけ繰り返すことで、指定したパスに存在する全てのﾌｧｲﾙ(ﾃﾞｨﾚｸﾄﾘ)の数だけ繰り返される。
		for (int i = 0; i < files.length; i++) {

			//【エラー処理】ﾌｧｲﾙなのか確認。//ファイル名を取得。ﾌｧｲﾙ名が8桁の場合取り出す。
			if (files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {

				//売上ファイルの条件に当てはまったﾌｧｲﾙだけ、List(ArrayList) に追加
				rcdFiles.add(files[i]);
			}
		}
		//【エラー処理】ﾌｧｲﾙ名が連番になっているか確認。
		Collections.sort(rcdFiles);
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if ((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		//ﾌｧｲﾙをﾌｧｲﾙの数だけ
		for (int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;
			try {


				//rcdFiles　値を引数として受け取る。FileReaderオブジェクトを生成。
				FileReader rcdfr = new FileReader(rcdFiles.get(i));

				//FileReader オブジェクトを引数として、そこから文字列を受け取るBufferedReader オブジェクトを生成。
				br = new BufferedReader(rcdfr);

				//売上ﾌｧｲﾙの中身のリスト作成。
				List<String> rcdDataList = new ArrayList<>();

				//文字列の行のﾌｨｰﾙﾄﾞ。保管。
				String line;

				// ファイル1行読み込んだら繰り返す

				//売上を新しいファイルに格納
				while ((line = br.readLine()) != null) {
					rcdDataList.add(line);
				}

				if (rcdDataList.size() != 2) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}

				//【エラー処理】支店ｺｰﾄﾞ、フォーマット確認。
				if (!branchSales.containsKey(rcdDataList.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}

				//【エラー処理】売上金額数字なのか確認
				if (!rcdDataList.get(1).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				//売上額をロング型に変換する(rcdDataList.get(0)　=　支店ｺｰﾄﾞ,rcdDataList.get(1)　=　売上金額)
				long fileSale = Long.parseLong(rcdDataList.get(1));

				//読み込んだ値を支店ｺｰﾄﾞ別に足し算し合計を出す。
				Long saleAmount = branchSales.get(rcdDataList.get(0)) + fileSale;

				//エラー処理。合計金額10桁超えたらｴﾗｰﾒｯｾｰｼﾞ。
				if (saleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				//出した合計をMapにしまう。
				branchSales.put(rcdDataList.get(0), saleAmount);

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
					}
				}
			}
		}

		if (!output(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}
	}

	//ﾒｿｯﾄﾞ分け開始位置
	public static boolean input(String path, String fileName, String checkFile, String regex, Map<String, String> names, Map<String, Long> sales) {

		//一時的なデータ置き場。
		BufferedReader br = null;

		try {

			//ﾌｧｲﾙｸﾗｽで.lstﾌｧｲﾙを指定
			File file = new File(path, fileName);

			//【エラー処理】
			if (!file.exists()) {
				System.out.println(checkFile + "ファイルが存在しません");
				return false;
			}

			//fileを引数としてfrに文字列を受け取る
			FileReader fr = new FileReader(file);

			//frを引数として文字列を受け取る
			br = new BufferedReader(fr);

			//文字列の行のﾌｨｰﾙﾄﾞ。保管
			String line;

			//ファイル1行読み込んだら繰り返す
			while ((line = br.readLine()) != null) {

				//1行ずつ読み込みカンマ区切りで配列に格納(分割した結果の配列が戻ってくる)
				String[] items = line.split(",");

				//【エラー処理】ﾌｧｲﾙのﾌｫｰﾏｯﾄを確認
				if ((items.length != 2) || (!items[0].matches(regex))) {
					System.out.println(checkFile + "ファイルのフォーマットが不正です");
					return false;
				}
				//値を格納(支店ｺｰﾄﾞ、支店名)
				names.put(items[0], items[1]);

				//値を格納(支店ｺｰﾄﾞ、売上金)
				sales.put(items[0], 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	public static boolean output(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {

		BufferedWriter bw = null;
		try {

			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for (String key : sales.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}